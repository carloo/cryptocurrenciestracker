var aCryptoXchanges = [];
//My exchange
var aGemini={};
aGemini.name="gemini";
aGemini.url="https://api.gemini.com/v1/pubticker/";
aGemini.pair={"BTCUSD":"btcusd", "ETHUSD":"ethusd"};
aGemini.constructUrl=function(iCryptoPair){
return aGemini.url + aGemini.pair[iCryptoPair]};
aGemini.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.last};
aCryptoXchanges.push(aGemini);

var aKraken={};
aKraken.name="kraken";
aKraken.url="https://api.kraken.com/0/public/Ticker?pair=";
aKraken.pair={"BTCUSD":"XXBTZUSD", "ETHUSD":"XETHZUSD", "XRPUSD":"XXRPZUSD"};
aKraken.constructUrl=function(iCryptoPair){
return aKraken.url + aKraken.pair[iCryptoPair]};
aKraken.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.result[aKraken.pair[iCryptoPair]].c[0]};
aCryptoXchanges.push(aKraken);

var aGdax={};
aGdax.name="gdax";
aGdax.url="https://api.gdax.com/products/";
aGdax.pair={"BTCUSD":"BTC-USD", "ETHUSD":"ETH-USD", "LTCUSD":"LTC-USD"};
aGdax.constructUrl=function(iCryptoPair){
return aGdax.url + aGdax.pair[iCryptoPair] + "/ticker"};
aGdax.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.price};
aCryptoXchanges.push(aGdax);

var aBitfinex={};
aBitfinex.name="bitfinex";
aBitfinex.url="https://api.bitfinex.com/v1/pubticker/";
aBitfinex.pair={"BTCUSD":"btcusd", "ETHUSD":"ethusd", "XRPUSD":"xrpusd"};
aBitfinex.constructUrl=function(iCryptoPair){
return aBitfinex.url + aBitfinex.pair[iCryptoPair]};
aBitfinex.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.last_price};
aCryptoXchanges.push(aBitfinex);

var aPoloniex={};
aPoloniex.name="poloniex";
aPoloniex.url="https://poloniex.com/public?command=returnTicker";
aPoloniex.pair={"BTCUSD":"USDT_BTC", "ETHUSD":"USDT_ETH", "XRPUSD":"USDT_XRP"};
aPoloniex.constructUrl=function(iCryptoPair){
return aPoloniex.url};
aPoloniex.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse[aPoloniex.pair[iCryptoPair]].last};
aCryptoXchanges.push(aPoloniex);

var aVersion="8";

/**
 * A special function that runs when the spreadsheet is open, used to add a
 * custom menu to the spreadsheet.
 */
function onOpen() {}

/**
 * Retrieve value of cryptocurrency.
 *
 * @param {string} iProvider provider to use (kraken, gemini, gdax, bitfinex, or poloniex).
 * @param {string} iCryptoPair crypto pair to retrieve (BTCUSD, ETHUSD, or XRPUSD).
 * @return The cryptocurrency value in desired fiat.
 * @example GET_CRYPTO("gemini"; "BTCUSD")
 * @customfunction
 */
function GET_CRYPTO(iProvider, iCryptoPair) {
  // Generic entry point for GET.
  //Logger.log("Entering GET_CRYPTO for provider: ", iProvider, " and pair: ", iCryptoPair);
  var aValue="0";
  //Before starting the real process we check if the iCryptoPair exists for the iProvider
  if(isKeyValidForProvider(iCryptoPair, iProvider) == false){
    throw new Error( "Error - unknow iCryptoPair: " + iCryptoPair + " for the provider: "+ iProvider);
  }
  aProviderObj=getProvider(iProvider);
  var aUrl = aProviderObj.constructUrl(iCryptoPair);
  var aResponse = UrlFetchApp.fetch(aUrl);
  var aResponseString = aResponse.getContentText();
  var aResponseJson = JSON.parse(aResponseString);
  //Logger.log(aResponseJson);
  //Parse the json response with the provider parser
  var aValue = aProviderObj.parseResponse(aResponseJson, iCryptoPair);
  //We need to convert as a int
  var aValueAsFloat = parseFloat(aValue)
  return aValueAsFloat;
}

/**
 * Retrieve the version of the script.
 *
 * @return the version of the script.
 * @example GET_CRYPTO()
 * @customfunction
 */
function GET_VERSION() {
  return aVersion;
}
